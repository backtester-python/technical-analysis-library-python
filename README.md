# Technical-Analysis-Library-Python
Python 3.7.x+ Technical Analysis Library.
* Candlestick Bars Pattern
* Chart Patterns
* Technical Indicators

## Getting Started

### Installation
#### Ubuntu 18.04 (Debian-based Linux)
```shell script
python3.7 -m pip install git+ssh://git@gitlab.com/<path>

python3.7 -m pip install git+ssh://git@gitlab.com/backtester-python/technical-analysis-library-python
```
```shell script
python3.7 -m pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>

python3.7 -m pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/backtester-python/technical-analysis-library-python
```
#### Windows 10
```shell script
pip install git+ssh://git@gitlab.com/<path>

pip install git+ssh://git@gitlab.com/backtester-python/technical-analysis-library-python
```
```shell script
pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>

pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/backtester-python/technical-analysis-library-python
```
#### Requirements.txt
For development of other projects
```
technicalanalysispy@ git+ssh://git@gitlab.com/backtester-python/technical-analysis-library-python
```

### Usage
#### Candlestick
```python
import pandas as pd
from technicalanalysispy.candlestick import Candlestick

file_path = './test/HSIF_LiquidityCorrected(Non-adjusted).csv'   # the path of the file.
df = pd.read_csv(file_path)     # df should consist of Date, Open, High, Low, Close, Volume
candle = Candlestick(df=df)
print(candle.o[4], candle.h[7], candle.l[2], candle.c[90])
print(candle.is_gap_down(9))
```

#### Technical Indicators
```python
import pandas as pd
from technicalanalysispy.technical_indicators import simple_moving_average

file_path = './test/HSIF_LiquidityCorrected(Non-adjusted).csv'   # the path of the file.
df = pd.read_csv(file_path)     # df should consist of Date, Open, High, Low, Close, Volume

sma_10 = simple_moving_average(ts_price=df.Close, lookback_window=10)
sma5_generator = simple_moving_average(ts_price=df.Close, lookback_window=5, fmt='generator')
print(next(sma5_generator))
```

#### Technical-Analysis-Library-Python & mplfinance
* [Matplotlib Finance](https://github.com/matplotlib/mplfinance)
```python
import pandas as pd
import mplfinance as mpf

file_path = './test/HSIF_LiquidityCorrected(Non-adjusted).csv'   # the path of the file.
df = pd.read_csv(file_path)     # df should consist of Date, Open, High, Low, Close, Volume
# Make sure the index is named as Date

mpf.plot(df,type='candle')
```

#### Chart Patterns
```python

```
