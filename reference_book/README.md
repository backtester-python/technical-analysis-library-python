## Reference Books

### Candlestick
* Encyclopedia Of Candlestick Charts.pdf (Author: Thomas Bulkowski)

### Chart Patterns
* Encyclopedia Of Chart Patterns, 2nd Edition.pdf (Author: Thomas Bulkowski)
